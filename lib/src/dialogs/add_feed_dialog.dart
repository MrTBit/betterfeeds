import 'package:flutter/material.dart';
import 'package:simple_feeds/src/enums/feed_type.dart';

class AddFeedDialog extends StatefulWidget {
  const AddFeedDialog({Key? key}) : super(key: key);

  @override
  _AddFeedDialogState createState() => _AddFeedDialogState();
}

class _AddFeedDialogState extends State<AddFeedDialog> {
  late TextEditingController _controller;

  FeedType? _addFeedType = FeedType.RSS;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: const EdgeInsets.all(5),
      title: const Text('Add Feed'),
      children: [
        Row(
          children: [
            Expanded(
                flex: 1,
                child: RadioListTile(
                    value: FeedType.RSS,
                    title: const Text('RSS'),
                    groupValue: _addFeedType,
                    onChanged: (FeedType? value) {
                      setState(() {
                        _addFeedType = value;
                      });
                    })),
            Expanded(
                flex: 1,
                child: RadioListTile(
                    value: FeedType.ATOM,
                    title: const Text('ATOM'),
                    groupValue: _addFeedType,
                    onChanged: (FeedType? value) {
                      setState(() {
                        _addFeedType = value;
                      });
                    }))
          ],
        ),
        Row(
          children: [
            Expanded(
                child: TextField(
                    autofocus: true,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(), labelText: 'URL'),
                    controller: _controller,
                    onSubmitted: (String value) => Navigator.pop(
                          context,
                          AddFeedDialogModel(value, _addFeedType),
                        )))
          ],
        ),
        Container(
          margin: const EdgeInsets.only(top: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                  margin: const EdgeInsets.only(right: 5),
                  child: ElevatedButton(
                      onPressed: () => Navigator.pop(context, null),
                      child: const Text('Cancel'))),
              ElevatedButton(
                  onPressed: () => Navigator.pop(context,
                      AddFeedDialogModel(_controller.value.text, _addFeedType)),
                  child: const Text('Add')),
            ],
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class AddFeedDialogModel {
  String url;
  FeedType? feedType;

  AddFeedDialogModel(this.url, this.feedType);
}
