// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tables.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Feed extends DataClass implements Insertable<Feed> {
  final int id;
  final String title;
  final String? description;
  final String link;
  final String url;
  final String? image;
  final FeedType feedType;
  Feed(
      {required this.id,
      required this.title,
      this.description,
      required this.link,
      required this.url,
      this.image,
      required this.feedType});
  factory Feed.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Feed(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title'])!,
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description']),
      link: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}link'])!,
      url: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}url'])!,
      image: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image']),
      feedType: $FeedsTable.$converter0.mapToDart(const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}feed_type']))!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['title'] = Variable<String>(title);
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String?>(description);
    }
    map['link'] = Variable<String>(link);
    map['url'] = Variable<String>(url);
    if (!nullToAbsent || image != null) {
      map['image'] = Variable<String?>(image);
    }
    {
      final converter = $FeedsTable.$converter0;
      map['feed_type'] = Variable<int>(converter.mapToSql(feedType)!);
    }
    return map;
  }

  FeedsCompanion toCompanion(bool nullToAbsent) {
    return FeedsCompanion(
      id: Value(id),
      title: Value(title),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      link: Value(link),
      url: Value(url),
      image:
          image == null && nullToAbsent ? const Value.absent() : Value(image),
      feedType: Value(feedType),
    );
  }

  factory Feed.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Feed(
      id: serializer.fromJson<int>(json['id']),
      title: serializer.fromJson<String>(json['title']),
      description: serializer.fromJson<String?>(json['description']),
      link: serializer.fromJson<String>(json['link']),
      url: serializer.fromJson<String>(json['url']),
      image: serializer.fromJson<String?>(json['image']),
      feedType: serializer.fromJson<FeedType>(json['feedType']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'title': serializer.toJson<String>(title),
      'description': serializer.toJson<String?>(description),
      'link': serializer.toJson<String>(link),
      'url': serializer.toJson<String>(url),
      'image': serializer.toJson<String?>(image),
      'feedType': serializer.toJson<FeedType>(feedType),
    };
  }

  Feed copyWith(
          {int? id,
          String? title,
          String? description,
          String? link,
          String? url,
          String? image,
          FeedType? feedType}) =>
      Feed(
        id: id ?? this.id,
        title: title ?? this.title,
        description: description ?? this.description,
        link: link ?? this.link,
        url: url ?? this.url,
        image: image ?? this.image,
        feedType: feedType ?? this.feedType,
      );
  @override
  String toString() {
    return (StringBuffer('Feed(')
          ..write('id: $id, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('link: $link, ')
          ..write('url: $url, ')
          ..write('image: $image, ')
          ..write('feedType: $feedType')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, title, description, link, url, image, feedType);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Feed &&
          other.id == this.id &&
          other.title == this.title &&
          other.description == this.description &&
          other.link == this.link &&
          other.url == this.url &&
          other.image == this.image &&
          other.feedType == this.feedType);
}

class FeedsCompanion extends UpdateCompanion<Feed> {
  final Value<int> id;
  final Value<String> title;
  final Value<String?> description;
  final Value<String> link;
  final Value<String> url;
  final Value<String?> image;
  final Value<FeedType> feedType;
  const FeedsCompanion({
    this.id = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.link = const Value.absent(),
    this.url = const Value.absent(),
    this.image = const Value.absent(),
    this.feedType = const Value.absent(),
  });
  FeedsCompanion.insert({
    this.id = const Value.absent(),
    required String title,
    this.description = const Value.absent(),
    required String link,
    required String url,
    this.image = const Value.absent(),
    required FeedType feedType,
  })  : title = Value(title),
        link = Value(link),
        url = Value(url),
        feedType = Value(feedType);
  static Insertable<Feed> custom({
    Expression<int>? id,
    Expression<String>? title,
    Expression<String?>? description,
    Expression<String>? link,
    Expression<String>? url,
    Expression<String?>? image,
    Expression<FeedType>? feedType,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (link != null) 'link': link,
      if (url != null) 'url': url,
      if (image != null) 'image': image,
      if (feedType != null) 'feed_type': feedType,
    });
  }

  FeedsCompanion copyWith(
      {Value<int>? id,
      Value<String>? title,
      Value<String?>? description,
      Value<String>? link,
      Value<String>? url,
      Value<String?>? image,
      Value<FeedType>? feedType}) {
    return FeedsCompanion(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
      link: link ?? this.link,
      url: url ?? this.url,
      image: image ?? this.image,
      feedType: feedType ?? this.feedType,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String?>(description.value);
    }
    if (link.present) {
      map['link'] = Variable<String>(link.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    if (image.present) {
      map['image'] = Variable<String?>(image.value);
    }
    if (feedType.present) {
      final converter = $FeedsTable.$converter0;
      map['feed_type'] = Variable<int>(converter.mapToSql(feedType.value)!);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FeedsCompanion(')
          ..write('id: $id, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('link: $link, ')
          ..write('url: $url, ')
          ..write('image: $image, ')
          ..write('feedType: $feedType')
          ..write(')'))
        .toString();
  }
}

class $FeedsTable extends Feeds with TableInfo<$FeedsTable, Feed> {
  final GeneratedDatabase _db;
  final String? _alias;
  $FeedsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  @override
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _linkMeta = const VerificationMeta('link');
  @override
  late final GeneratedColumn<String?> link = GeneratedColumn<String?>(
      'link', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _urlMeta = const VerificationMeta('url');
  @override
  late final GeneratedColumn<String?> url = GeneratedColumn<String?>(
      'url', aliasedName, false,
      type: const StringType(), requiredDuringInsert: true);
  final VerificationMeta _imageMeta = const VerificationMeta('image');
  @override
  late final GeneratedColumn<String?> image = GeneratedColumn<String?>(
      'image', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _feedTypeMeta = const VerificationMeta('feedType');
  @override
  late final GeneratedColumnWithTypeConverter<FeedType, int?> feedType =
      GeneratedColumn<int?>('feed_type', aliasedName, false,
              type: const IntType(), requiredDuringInsert: true)
          .withConverter<FeedType>($FeedsTable.$converter0);
  @override
  List<GeneratedColumn> get $columns =>
      [id, title, description, link, url, image, feedType];
  @override
  String get aliasedName => _alias ?? 'feeds';
  @override
  String get actualTableName => 'feeds';
  @override
  VerificationContext validateIntegrity(Insertable<Feed> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('link')) {
      context.handle(
          _linkMeta, link.isAcceptableOrUnknown(data['link']!, _linkMeta));
    } else if (isInserting) {
      context.missing(_linkMeta);
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url']!, _urlMeta));
    } else if (isInserting) {
      context.missing(_urlMeta);
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image']!, _imageMeta));
    }
    context.handle(_feedTypeMeta, const VerificationResult.success());
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Feed map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Feed.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $FeedsTable createAlias(String alias) {
    return $FeedsTable(_db, alias);
  }

  static TypeConverter<FeedType, int> $converter0 =
      const EnumIndexConverter<FeedType>(FeedType.values);
}

class FeedItem extends DataClass implements Insertable<FeedItem> {
  final int id;
  final String? title;
  final String? description;
  final String? link;
  final String? guid;
  final DateTime? publishDate;
  final DateTime dateReceived;
  final bool isRead;
  final bool isFavorite;
  final int feedId;
  FeedItem(
      {required this.id,
      this.title,
      this.description,
      this.link,
      this.guid,
      this.publishDate,
      required this.dateReceived,
      required this.isRead,
      required this.isFavorite,
      required this.feedId});
  factory FeedItem.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return FeedItem(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      title: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}title']),
      description: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}description']),
      link: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}link']),
      guid: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}guid']),
      publishDate: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}publish_date']),
      dateReceived: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}date_received'])!,
      isRead: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_read'])!,
      isFavorite: const BoolType()
          .mapFromDatabaseResponse(data['${effectivePrefix}is_favorite'])!,
      feedId: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}feed_id'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    if (!nullToAbsent || title != null) {
      map['title'] = Variable<String?>(title);
    }
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String?>(description);
    }
    if (!nullToAbsent || link != null) {
      map['link'] = Variable<String?>(link);
    }
    if (!nullToAbsent || guid != null) {
      map['guid'] = Variable<String?>(guid);
    }
    if (!nullToAbsent || publishDate != null) {
      map['publish_date'] = Variable<DateTime?>(publishDate);
    }
    map['date_received'] = Variable<DateTime>(dateReceived);
    map['is_read'] = Variable<bool>(isRead);
    map['is_favorite'] = Variable<bool>(isFavorite);
    map['feed_id'] = Variable<int>(feedId);
    return map;
  }

  FeedItemsCompanion toCompanion(bool nullToAbsent) {
    return FeedItemsCompanion(
      id: Value(id),
      title:
          title == null && nullToAbsent ? const Value.absent() : Value(title),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      link: link == null && nullToAbsent ? const Value.absent() : Value(link),
      guid: guid == null && nullToAbsent ? const Value.absent() : Value(guid),
      publishDate: publishDate == null && nullToAbsent
          ? const Value.absent()
          : Value(publishDate),
      dateReceived: Value(dateReceived),
      isRead: Value(isRead),
      isFavorite: Value(isFavorite),
      feedId: Value(feedId),
    );
  }

  factory FeedItem.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FeedItem(
      id: serializer.fromJson<int>(json['id']),
      title: serializer.fromJson<String?>(json['title']),
      description: serializer.fromJson<String?>(json['description']),
      link: serializer.fromJson<String?>(json['link']),
      guid: serializer.fromJson<String?>(json['guid']),
      publishDate: serializer.fromJson<DateTime?>(json['publishDate']),
      dateReceived: serializer.fromJson<DateTime>(json['dateReceived']),
      isRead: serializer.fromJson<bool>(json['isRead']),
      isFavorite: serializer.fromJson<bool>(json['isFavorite']),
      feedId: serializer.fromJson<int>(json['feedId']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'title': serializer.toJson<String?>(title),
      'description': serializer.toJson<String?>(description),
      'link': serializer.toJson<String?>(link),
      'guid': serializer.toJson<String?>(guid),
      'publishDate': serializer.toJson<DateTime?>(publishDate),
      'dateReceived': serializer.toJson<DateTime>(dateReceived),
      'isRead': serializer.toJson<bool>(isRead),
      'isFavorite': serializer.toJson<bool>(isFavorite),
      'feedId': serializer.toJson<int>(feedId),
    };
  }

  FeedItem copyWith(
          {int? id,
          String? title,
          String? description,
          String? link,
          String? guid,
          DateTime? publishDate,
          DateTime? dateReceived,
          bool? isRead,
          bool? isFavorite,
          int? feedId}) =>
      FeedItem(
        id: id ?? this.id,
        title: title ?? this.title,
        description: description ?? this.description,
        link: link ?? this.link,
        guid: guid ?? this.guid,
        publishDate: publishDate ?? this.publishDate,
        dateReceived: dateReceived ?? this.dateReceived,
        isRead: isRead ?? this.isRead,
        isFavorite: isFavorite ?? this.isFavorite,
        feedId: feedId ?? this.feedId,
      );
  @override
  String toString() {
    return (StringBuffer('FeedItem(')
          ..write('id: $id, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('link: $link, ')
          ..write('guid: $guid, ')
          ..write('publishDate: $publishDate, ')
          ..write('dateReceived: $dateReceived, ')
          ..write('isRead: $isRead, ')
          ..write('isFavorite: $isFavorite, ')
          ..write('feedId: $feedId')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, title, description, link, guid,
      publishDate, dateReceived, isRead, isFavorite, feedId);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FeedItem &&
          other.id == this.id &&
          other.title == this.title &&
          other.description == this.description &&
          other.link == this.link &&
          other.guid == this.guid &&
          other.publishDate == this.publishDate &&
          other.dateReceived == this.dateReceived &&
          other.isRead == this.isRead &&
          other.isFavorite == this.isFavorite &&
          other.feedId == this.feedId);
}

class FeedItemsCompanion extends UpdateCompanion<FeedItem> {
  final Value<int> id;
  final Value<String?> title;
  final Value<String?> description;
  final Value<String?> link;
  final Value<String?> guid;
  final Value<DateTime?> publishDate;
  final Value<DateTime> dateReceived;
  final Value<bool> isRead;
  final Value<bool> isFavorite;
  final Value<int> feedId;
  const FeedItemsCompanion({
    this.id = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.link = const Value.absent(),
    this.guid = const Value.absent(),
    this.publishDate = const Value.absent(),
    this.dateReceived = const Value.absent(),
    this.isRead = const Value.absent(),
    this.isFavorite = const Value.absent(),
    this.feedId = const Value.absent(),
  });
  FeedItemsCompanion.insert({
    this.id = const Value.absent(),
    this.title = const Value.absent(),
    this.description = const Value.absent(),
    this.link = const Value.absent(),
    this.guid = const Value.absent(),
    this.publishDate = const Value.absent(),
    this.dateReceived = const Value.absent(),
    this.isRead = const Value.absent(),
    this.isFavorite = const Value.absent(),
    required int feedId,
  }) : feedId = Value(feedId);
  static Insertable<FeedItem> custom({
    Expression<int>? id,
    Expression<String?>? title,
    Expression<String?>? description,
    Expression<String?>? link,
    Expression<String?>? guid,
    Expression<DateTime?>? publishDate,
    Expression<DateTime>? dateReceived,
    Expression<bool>? isRead,
    Expression<bool>? isFavorite,
    Expression<int>? feedId,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (title != null) 'title': title,
      if (description != null) 'description': description,
      if (link != null) 'link': link,
      if (guid != null) 'guid': guid,
      if (publishDate != null) 'publish_date': publishDate,
      if (dateReceived != null) 'date_received': dateReceived,
      if (isRead != null) 'is_read': isRead,
      if (isFavorite != null) 'is_favorite': isFavorite,
      if (feedId != null) 'feed_id': feedId,
    });
  }

  FeedItemsCompanion copyWith(
      {Value<int>? id,
      Value<String?>? title,
      Value<String?>? description,
      Value<String?>? link,
      Value<String?>? guid,
      Value<DateTime?>? publishDate,
      Value<DateTime>? dateReceived,
      Value<bool>? isRead,
      Value<bool>? isFavorite,
      Value<int>? feedId}) {
    return FeedItemsCompanion(
      id: id ?? this.id,
      title: title ?? this.title,
      description: description ?? this.description,
      link: link ?? this.link,
      guid: guid ?? this.guid,
      publishDate: publishDate ?? this.publishDate,
      dateReceived: dateReceived ?? this.dateReceived,
      isRead: isRead ?? this.isRead,
      isFavorite: isFavorite ?? this.isFavorite,
      feedId: feedId ?? this.feedId,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (title.present) {
      map['title'] = Variable<String?>(title.value);
    }
    if (description.present) {
      map['description'] = Variable<String?>(description.value);
    }
    if (link.present) {
      map['link'] = Variable<String?>(link.value);
    }
    if (guid.present) {
      map['guid'] = Variable<String?>(guid.value);
    }
    if (publishDate.present) {
      map['publish_date'] = Variable<DateTime?>(publishDate.value);
    }
    if (dateReceived.present) {
      map['date_received'] = Variable<DateTime>(dateReceived.value);
    }
    if (isRead.present) {
      map['is_read'] = Variable<bool>(isRead.value);
    }
    if (isFavorite.present) {
      map['is_favorite'] = Variable<bool>(isFavorite.value);
    }
    if (feedId.present) {
      map['feed_id'] = Variable<int>(feedId.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FeedItemsCompanion(')
          ..write('id: $id, ')
          ..write('title: $title, ')
          ..write('description: $description, ')
          ..write('link: $link, ')
          ..write('guid: $guid, ')
          ..write('publishDate: $publishDate, ')
          ..write('dateReceived: $dateReceived, ')
          ..write('isRead: $isRead, ')
          ..write('isFavorite: $isFavorite, ')
          ..write('feedId: $feedId')
          ..write(')'))
        .toString();
  }
}

class $FeedItemsTable extends FeedItems
    with TableInfo<$FeedItemsTable, FeedItem> {
  final GeneratedDatabase _db;
  final String? _alias;
  $FeedItemsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _titleMeta = const VerificationMeta('title');
  @override
  late final GeneratedColumn<String?> title = GeneratedColumn<String?>(
      'title', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String?> description = GeneratedColumn<String?>(
      'description', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _linkMeta = const VerificationMeta('link');
  @override
  late final GeneratedColumn<String?> link = GeneratedColumn<String?>(
      'link', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _guidMeta = const VerificationMeta('guid');
  @override
  late final GeneratedColumn<String?> guid = GeneratedColumn<String?>(
      'guid', aliasedName, true,
      type: const StringType(), requiredDuringInsert: false);
  final VerificationMeta _publishDateMeta =
      const VerificationMeta('publishDate');
  @override
  late final GeneratedColumn<DateTime?> publishDate =
      GeneratedColumn<DateTime?>('publish_date', aliasedName, true,
          type: const IntType(), requiredDuringInsert: false);
  final VerificationMeta _dateReceivedMeta =
      const VerificationMeta('dateReceived');
  @override
  late final GeneratedColumn<DateTime?> dateReceived =
      GeneratedColumn<DateTime?>('date_received', aliasedName, false,
          type: const IntType(),
          requiredDuringInsert: false,
          clientDefault: () => DateTime.now());
  final VerificationMeta _isReadMeta = const VerificationMeta('isRead');
  @override
  late final GeneratedColumn<bool?> isRead = GeneratedColumn<bool?>(
      'is_read', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_read IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _isFavoriteMeta = const VerificationMeta('isFavorite');
  @override
  late final GeneratedColumn<bool?> isFavorite = GeneratedColumn<bool?>(
      'is_favorite', aliasedName, false,
      type: const BoolType(),
      requiredDuringInsert: false,
      defaultConstraints: 'CHECK (is_favorite IN (0, 1))',
      defaultValue: const Constant(false));
  final VerificationMeta _feedIdMeta = const VerificationMeta('feedId');
  @override
  late final GeneratedColumn<int?> feedId = GeneratedColumn<int?>(
      'feed_id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: true,
      defaultConstraints: 'REFERENCES feeds (id)');
  @override
  List<GeneratedColumn> get $columns => [
        id,
        title,
        description,
        link,
        guid,
        publishDate,
        dateReceived,
        isRead,
        isFavorite,
        feedId
      ];
  @override
  String get aliasedName => _alias ?? 'feed_items';
  @override
  String get actualTableName => 'feed_items';
  @override
  VerificationContext validateIntegrity(Insertable<FeedItem> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title']!, _titleMeta));
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('link')) {
      context.handle(
          _linkMeta, link.isAcceptableOrUnknown(data['link']!, _linkMeta));
    }
    if (data.containsKey('guid')) {
      context.handle(
          _guidMeta, guid.isAcceptableOrUnknown(data['guid']!, _guidMeta));
    }
    if (data.containsKey('publish_date')) {
      context.handle(
          _publishDateMeta,
          publishDate.isAcceptableOrUnknown(
              data['publish_date']!, _publishDateMeta));
    }
    if (data.containsKey('date_received')) {
      context.handle(
          _dateReceivedMeta,
          dateReceived.isAcceptableOrUnknown(
              data['date_received']!, _dateReceivedMeta));
    }
    if (data.containsKey('is_read')) {
      context.handle(_isReadMeta,
          isRead.isAcceptableOrUnknown(data['is_read']!, _isReadMeta));
    }
    if (data.containsKey('is_favorite')) {
      context.handle(
          _isFavoriteMeta,
          isFavorite.isAcceptableOrUnknown(
              data['is_favorite']!, _isFavoriteMeta));
    }
    if (data.containsKey('feed_id')) {
      context.handle(_feedIdMeta,
          feedId.isAcceptableOrUnknown(data['feed_id']!, _feedIdMeta));
    } else if (isInserting) {
      context.missing(_feedIdMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FeedItem map(Map<String, dynamic> data, {String? tablePrefix}) {
    return FeedItem.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $FeedItemsTable createAlias(String alias) {
    return $FeedItemsTable(_db, alias);
  }
}

abstract class _$SimpleFeedsDatabase extends GeneratedDatabase {
  _$SimpleFeedsDatabase(QueryExecutor e)
      : super(SqlTypeSystem.defaultInstance, e);
  late final $FeedsTable feeds = $FeedsTable(this);
  late final $FeedItemsTable feedItems = $FeedItemsTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [feeds, feedItems];
}
