import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simple_feeds/src/enums/feed_type.dart';
import 'package:path/path.dart' as p;

part 'tables.g.dart';

class Feeds extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get title => text()();

  TextColumn get description => text().nullable()();

  TextColumn get link => text()();

  TextColumn get url => text()();

  TextColumn get image => text().nullable()();

  IntColumn get feedType => intEnum<FeedType>()();
}

class FeedItems extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get title => text().nullable()();

  TextColumn get description => text().nullable()();

  TextColumn get link => text().nullable()();

  TextColumn get guid => text().nullable()();

  DateTimeColumn get publishDate => dateTime().nullable()();

  DateTimeColumn get dateReceived =>
      dateTime().clientDefault(() => DateTime.now())();

  BoolColumn get isRead => boolean().withDefault(const Constant(false))();

  BoolColumn get isFavorite => boolean().withDefault(const Constant(false))();

  IntColumn get feedId => integer().references(Feeds, #id)();
}

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'betterfeedsdb.sqlite'));
    return NativeDatabase(file);
  });
}

@DriftDatabase(tables: [Feeds, FeedItems])
class SimpleFeedsDatabase extends _$SimpleFeedsDatabase {
  SimpleFeedsDatabase() : super(_openConnection());

  // you should bump this number whenever you change or add a table definition.
  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration =>
      MigrationStrategy(beforeOpen: (details) async {
        await customStatement('PRAGMA foreign_keys = ON');
      });

  Future<List<Feed>> get getAllFeeds =>
      (select(feeds)..orderBy([(t) => OrderingTerm(expression: t.title)]))
          .get();

  Future<Feed?> findFeedByUrl(String url) =>
      (select(feeds)..where((f) => f.url.equals(url))).getSingleOrNull();

  Future<int> addFeed(FeedsCompanion feed) {
    return into(feeds).insert(feed);
  }

  Future updateFeed(Feed entry) {
    // using replace will update all fields from the entry that are not marked as a primary key.
    // it will also make sure that only the entry with the same primary key will be updated.
    // Here, this means that the row that has the same id as entry will be updated to reflect
    // the entry's title, content and category. As its where clause is set automatically, it
    // cannot be used together with where.
    return update(feeds).replace(entry);
  }

  Future removeFeed(int id) {
    return (delete(feeds)..where((feed) => feed.id.equals(id))).go();
  }

  Future removeFeedItemsForFeed(int feedId) {
    return (delete(feedItems)
          ..where((feedItem) => feedItem.feedId.equals(feedId)))
        .go();
  }

  Future<List<FeedItem>> get getAllFeedItems => (select(feedItems)
        ..orderBy([
          (t) =>
              OrderingTerm(expression: t.publishDate, mode: OrderingMode.desc),
          (t) =>
              OrderingTerm(expression: t.dateReceived, mode: OrderingMode.desc)
        ]))
      .get();

  Future<List<FeedItem>> getAllFeedItemsByFeedId(int feedId) => (select(
          feedItems)
        ..where((fi) => fi.feedId.equals(feedId))
        ..orderBy([
          (t) =>
              OrderingTerm(expression: t.publishDate, mode: OrderingMode.desc),
          (t) =>
              OrderingTerm(expression: t.dateReceived, mode: OrderingMode.desc)
        ]))
      .get();

  Future<List<FeedItem>> getUnreadFeedItems() => (select(feedItems)
        ..where((fi) => fi.isRead.equals(false))
        ..orderBy([
          (t) =>
              OrderingTerm(expression: t.publishDate, mode: OrderingMode.desc),
          (t) =>
              OrderingTerm(expression: t.dateReceived, mode: OrderingMode.desc)
        ]))
      .get();

  Future<FeedItem?> getFeedItemById(int id) =>
      (select(feedItems)..where((fi) => fi.id.equals(id))).getSingleOrNull();

  Future markFeedItemAsRead(int id) =>
      (update(feedItems)..where((fi) => fi.id.equals(id)))
          .write(const FeedItemsCompanion(isRead: Value(true)));

  Future<int> addFeedItem(FeedItemsCompanion feedItem) {
    return into(feedItems).insert(feedItem);
  }

  Future updateFeedItem(FeedItem entry) {
    return update(feedItems).replace(entry);
  }
}
