import 'dart:io';

import 'package:drift/drift.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simple_feeds/src/database/tables.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

class FileService {

  Future<Directory> _getFeedDirectory(int feedId) async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    Directory feedDirectory = Directory(join(documentDirectory.path, feedId.toString()));

    if (!await feedDirectory.exists()) {
      await feedDirectory.create();
    }

    return feedDirectory;
  }

  Future<Directory> _getFeedItemDirectory(int feedId, int feedItemId) async {
    Directory feedItemDirectory = Directory(join((await _getFeedDirectory(feedId)).path, feedItemId.toString()));

    if (!await feedItemDirectory.exists()) {
      await feedItemDirectory.create();
    }

    return feedItemDirectory;
  }

  Future saveFeedItemImage(Uint8List bytes, String imageIdentifier, int feedId, int feedItemId) async {
    Directory feedItemDirectory = await _getFeedItemDirectory(feedId, feedItemId);

    String hashedImageId = md5.convert(utf8.encode(imageIdentifier)).toString();

    File file = File(join(feedItemDirectory.path, hashedImageId));

    return file.writeAsBytes(bytes);
  }

  Future<File?> loadFeedItemImage(String imageIdentifier, int feedId, int feedItemId) async {
    Directory feedItemDirectory = await _getFeedItemDirectory(feedId, feedItemId);

    String hashedImageId = md5.convert(utf8.encode(imageIdentifier)).toString();

    File file = File(join(feedItemDirectory.path, hashedImageId));

    if (await file.exists()) {
      return file;
    } else {
      return null;
    }
  }

  Future<File?> getFeedItemListImage(FeedItem feedItem) async {
    Directory feedItemDirectory = await _getFeedItemDirectory(feedItem.feedId, feedItem.id);

    List<FileSystemEntity> fsItems = await feedItemDirectory.list().toList();

    List<File> files = fsItems.whereType<File>().toList();

    if (files.isNotEmpty) {
      return files.first;
    }

    File? defaultFeedImage = await loadFeedImage(feedItem.feedId);

    if (defaultFeedImage != null) {
      return defaultFeedImage;
    } else {
      return null;
    }
  }

  Future saveFeedImage(Uint8List bytes, int feedId) async {
    File file = File(join((await _getFeedDirectory(feedId)).path, 'main'));

    return file.writeAsBytes(bytes);
  }

  Future<File?> loadFeedImage(int feedId) async {
    File file = File(join((await _getFeedDirectory(feedId)).path, 'main'));

    if (await file.exists()) {
      return file;
    } else {
      return null;
    }
  }

  Future removeFeedFolder(int feedId) async {
    Directory feedDirectory = await _getFeedDirectory(feedId);

    if (await feedDirectory.exists()) {
      await feedDirectory.delete(recursive: true);
    }
  }

}