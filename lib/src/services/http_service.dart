import 'dart:io';

import 'package:http/http.dart';
import 'package:http/io_client.dart';

class HttpService {

  Future<Response> getResponse(String url) async {
    final client = IOClient(HttpClient()
      ..badCertificateCallback =
      ((X509Certificate cert, String host, int port) => true));

    url = _addHttp(url);

    Response response = await client.get(Uri.parse(url));
    client.close();

    return response;
  }

  String _addHttp(String url) {
    if (!url.toLowerCase().startsWith('http')) {
      return 'http://' + url;
    }

    return url;
  }
}