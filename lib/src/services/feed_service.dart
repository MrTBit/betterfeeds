
import 'dart:io';

import 'package:drift/drift.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart';
import 'package:simple_feeds/main.dart';
import 'package:simple_feeds/src/database/tables.dart';
import 'package:simple_feeds/src/enums/feed_type.dart';
import 'package:simple_feeds/src/models/refresh_feed_helper.dart';
import 'package:simple_feeds/src/models/simple_feed.dart';
import 'package:simple_feeds/src/models/simple_feed_item.dart';
import 'package:simple_feeds/src/services/file_service.dart';
import 'package:simple_feeds/src/services/http_service.dart';
import 'package:webfeed/domain/atom_feed.dart';
import 'package:webfeed/domain/rss_feed.dart';

class FeedService {
  late SimpleFeedsDatabase _db;
  late HttpService _httpService;
  late FileService _fileService;

  FeedService(){
    _db = getIt<SimpleFeedsDatabase>();
    _httpService = getIt<HttpService>();
    _fileService = getIt<FileService>();
  }

  Future<String?> addRssFeed(String url) async {
    try {

      RssFeed feed = await _loadRssFeed(url);

      String? image;

      FeedsCompanion feedsCompanion = FeedsCompanion(
        title: Value(feed.title ?? ""),
        description: Value(feed.description),
        feedType: const Value(FeedType.RSS),
        link: Value(feed.link ?? ""),
        url: Value(url),
        image: image != null ? Value(image) : const Value.absent(),
      );

      int feedId = await addFeedToDb(feedsCompanion);

      if (feed.image != null && feed.image!.url != null) {
        _saveFeedImage(feed.image!.url!, feedId);
      }

      List<FeedItemsCompanion> feedItems = feed.items
              ?.map((item) => FeedItemsCompanion(
                  link: Value(item.link),
                  description: Value(item.description),
                  title: Value(item.title),
                  dateReceived: Value(DateTime.now()),
                  guid: Value(item.guid),
                  isFavorite: const Value(false),
                  isRead: const Value(false),
                  publishDate: Value(item.pubDate),
                  feedId: Value(feedId)))
              .toList() ??
          List.empty();

      await addItemsToFeedInDb(feedItems);

      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future<String?> addAtomFeed(String url) async {
    try {
      AtomFeed feed = await _loadAtomFeed(url);

      FeedsCompanion feedsCompanion = FeedsCompanion(
        title: Value(feed.title ?? ""),
        feedType: const Value(FeedType.RSS),
        link: Value(feed.links?.first.href ?? ""),
        url: Value(url),
      );

      int feedId = await addFeedToDb(feedsCompanion);

      List<FeedItemsCompanion> feedItems = feed.items
              ?.map((item) => FeedItemsCompanion(
                  link: Value(item.links?.first.href),
                  description: Value(item.content),
                  title: Value(item.title),
                  dateReceived: Value(DateTime.now()),
                  isFavorite: const Value(false),
                  isRead: const Value(false),
                  publishDate: Value(item.updated),
                  feedId: Value(feedId)))
              .toList() ??
          List.empty();

      await addItemsToFeedInDb(feedItems);

      return null;
    } catch (e) {
      return e.toString();
    }
  }

  Future<RssFeed> _loadRssFeed(String url) async {
    Response response = await _httpService.getResponse(url);
    return RssFeed.parse(response.body);
  }

  Future<AtomFeed> _loadAtomFeed(String url) async {
    Response response = await _httpService.getResponse(url);
    return AtomFeed.parse(response.body);
  }

  Future<int> addFeedToDb(FeedsCompanion feed) async {
    SimpleFeedsDatabase db = getIt<SimpleFeedsDatabase>();
    return await db.addFeed(feed);
  }

  Future addItemsToFeedInDb(List<FeedItemsCompanion> feedItems) async {
    for (FeedItemsCompanion feedItem in feedItems) {
      int feedItemId = await _db.addFeedItem(feedItem);

      await _saveFeedItemImages(feedItem.description.value, feedItem.feedId.value, feedItemId);
    }
  }

  Future _saveFeedImage(String url, int feedId) async {
    Response response = await _httpService.getResponse(url);
    return _fileService.saveFeedImage(response.bodyBytes, feedId);
  }

  Future<File?> getFeedItemImage(FeedItem feedItem) {
    return _fileService.getFeedItemListImage(feedItem);
  }

  Future<File?> getSubscriptionImage(Feed feed) {
    return _fileService.loadFeedImage(feed.id);
  }

  Future removeSubscription(int id) async {
    await _db.removeFeedItemsForFeed(id);
    await _db.removeFeed(id);
    await _fileService.removeFeedFolder(id);
  }

  Future<List<FeedItem>> getAllFeedItems() async {
    return _db.getAllFeedItems;
  }

  Future<List<Feed>> getAllFeeds() async {
    return _db.getAllFeeds;
  }

  Future<List<FeedItem>> getUnreadFeedItems() {
    return _db.getUnreadFeedItems();
  }

  Future<FeedItem?> getFeedItemById(int id) async {
    FeedItem? fi = await _db.getFeedItemById(id);
    return fi;
  }
  
  Future markFeedItemAsRead(FeedItem feedItem) async {
    await _db.markFeedItemAsRead(feedItem.id);
  }

  Future<RefreshFeedHelper> refreshFeeds() async {
    List<Feed> allFeeds = await getAllFeeds();

    bool errorOccurred = false;

    int newFeedItems = 0;

    for (Feed feed in allFeeds) {
      SimpleFeed? fetchedFeed;

      if (feed.feedType == FeedType.ATOM) {

        try {
          fetchedFeed = SimpleFeed.fromAtomFeed(await _loadAtomFeed(feed.url), feed.url);
        } catch (e) {
          //if an error occurred, make note that it happened and move on
          errorOccurred = true;
          continue;
        }

      } else if (feed.feedType == FeedType.RSS) {

        try {
          fetchedFeed = SimpleFeed.fromRssFeed(await _loadRssFeed(feed.url), feed.url);
        } catch (e) {
          //if an error occurred, make note that it happened and move on
          errorOccurred = true;
          continue;
        }

      }

      if (fetchedFeed != null) {
        newFeedItems += await _checkForNewFeedItems(feed, fetchedFeed);
      }
    }

    return RefreshFeedHelper(newFeedItems, errorOccurred);
  }

  Future<int> _checkForNewFeedItems(Feed dbFeed, SimpleFeed fetchedFeed) async {
    List<FeedItem> dbFeedItems = await _db.getAllFeedItemsByFeedId(dbFeed.id);

    int newItems = 0;

    if (dbFeedItems.isEmpty) {
      for (SimpleFeedItem simpleFeedItem in fetchedFeed.simpleFeedItems) {
        newItems++;
        int feedItemId = await _db.addFeedItem(simpleFeedItem.convertToFeedItemsCompanion(dbFeed.id));
        _saveFeedItemImages(simpleFeedItem.description, dbFeed.id, feedItemId);
      }
    } else if (dbFeedItems.first.guid != null) {
      for (SimpleFeedItem simpleFeedItem in fetchedFeed.simpleFeedItems) {
        if (!dbFeedItems.any((fi) => fi.guid == simpleFeedItem.guid)) {
          newItems++;
          int feedItemId = await _db.addFeedItem(simpleFeedItem.convertToFeedItemsCompanion(dbFeed.id));
          _saveFeedItemImages(simpleFeedItem.description, dbFeed.id, feedItemId);
        }
      }
    } else if (dbFeedItems.first.link != null) {
      for (SimpleFeedItem simpleFeedItem in fetchedFeed.simpleFeedItems) {
        if (!dbFeedItems.any((fi) => fi.link == simpleFeedItem.link)) {
          newItems++;
          int feedItemId = await _db.addFeedItem(simpleFeedItem.convertToFeedItemsCompanion(dbFeed.id));
          _saveFeedItemImages(simpleFeedItem.description, dbFeed.id, feedItemId);
        }
      }
    } else if (dbFeedItems.first.title != null) {
      for (SimpleFeedItem simpleFeedItem in fetchedFeed.simpleFeedItems) {
        if (!dbFeedItems.any((fi) => fi.title == simpleFeedItem.title)) {
          newItems++;
          int feedItemId = await _db.addFeedItem(simpleFeedItem.convertToFeedItemsCompanion(dbFeed.id));
          _saveFeedItemImages(simpleFeedItem.description, dbFeed.id, feedItemId);
        }
      }
    } else if (dbFeedItems.first.description != null) {
      for (SimpleFeedItem simpleFeedItem in fetchedFeed.simpleFeedItems) {
        if (!dbFeedItems.any((fi) => fi.description == simpleFeedItem.description)) {
          newItems++;
          int feedItemId = await _db.addFeedItem(simpleFeedItem.convertToFeedItemsCompanion(dbFeed.id));
          _saveFeedItemImages(simpleFeedItem.description, dbFeed.id, feedItemId);
        }
      }
    }

    return newItems;
  }

  Future _saveFeedItemImages(String? html, int feedId, int feedItemId) async {
    if (html == null) {
      return;
    }

    try {
      Document document = parse(html);

      List<Element> imgTags = document.getElementsByTagName('img');
      
      for (Element imgElement in imgTags) {
        String? url = imgElement.attributes['src'];
        if (url != null) {
          _fileService.saveFeedItemImage((await _httpService.getResponse(url)).bodyBytes, url, feedId, feedItemId);
        }
      }
      
    } catch (e) {
      //do nothing, isn't html
    }
  }
}