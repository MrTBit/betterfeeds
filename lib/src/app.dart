import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:simple_feeds/main.dart';
import 'package:simple_feeds/src/routing/delegate.dart';
import 'package:simple_feeds/src/routing/parser.dart';
import 'package:simple_feeds/src/routing/route_state.dart';
import 'package:simple_feeds/src/screens/navigator.dart';
import 'package:simple_feeds/src/services/feed_service.dart';
import 'package:simple_feeds/src/services/notification_service.dart';

class SimpleFeeds extends StatefulWidget {
  const SimpleFeeds({Key? key}) : super(key: key);

  @override
  _SimpleFeedsState createState() => _SimpleFeedsState();
}

class _SimpleFeedsState extends State<SimpleFeeds> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  late final NotificationService _notificationService;
  late final RouteState _routeState;
  late final SimpleRouterDelegate _routerDelegate;
  late final TemplateRouteParser _routeParser;

  @override
  void initState() {
    _notificationService = getIt<NotificationService>();

    ///Configure the parser with all of the app's allowed path templates.
    _routeParser = TemplateRouteParser(allowedPaths: [
      '/feeds',
      '/feeds/:feedItemId',
      '/subscriptions',
      '/settings',
    ], initialRoute: '/feeds');

    _routeState = RouteState(_routeParser);

    _routerDelegate = SimpleRouterDelegate(
        routeState: _routeState,
        builder: (context) => SimpleFeedsNavigator(navigatorKey: _navigatorKey),
        navigatorKey: _navigatorKey);

    super.initState();
    initPlatformState();
  }

  @override
  Widget build(BuildContext context) => RouteStateScope(
        notifier: _routeState,
        child: MaterialApp.router(
          routerDelegate: _routerDelegate,
          routeInformationParser: _routeParser,
          theme: ThemeData(
            brightness: Brightness.light,
            pageTransitionsTheme: const PageTransitionsTheme(
              builders: {
                TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
              },
            ),
          ),
          darkTheme: ThemeData(
            brightness: Brightness.dark,
            pageTransitionsTheme: const PageTransitionsTheme(
              builders: {
                TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
              },
            ),
          ),
          themeMode: ThemeMode.system,
        ),
      );

  @override
  void dispose() {
    _routeState.dispose();
    _routerDelegate.dispose();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Configure BackgroundFetch.
    await BackgroundFetch.configure(
        BackgroundFetchConfig(
            startOnBoot: true,
            minimumFetchInterval: 15,
            stopOnTerminate: false,
            enableHeadless: true,
            requiresBatteryNotLow: false,
            requiresCharging: false,
            requiresStorageNotLow: false,
            requiresDeviceIdle: false,
            requiredNetworkType: NetworkType.ANY), (String taskId) async {
      // <-- Event handler
      // This is the fetch-event callback.
      int newFeedItems =
          (await getIt<FeedService>().refreshFeeds()).newFeedItems;
      if (newFeedItems > 0) {
        await _notificationService.displayNotification(newFeedItems);
      }
      // IMPORTANT:  You must signal completion of your task or the OS can punish your app
      // for taking too long in the background.
      BackgroundFetch.finish(taskId);
    }, (String taskId) async {
      // <-- Task timeout handler.
      // This task has exceeded its allowed running-time.  You must stop what you're doing and immediately .finish(taskId)
      BackgroundFetch.finish(taskId);
    });

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }
}
