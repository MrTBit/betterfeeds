class RefreshFeedHelper {
  final int newFeedItems;
  final bool errorOccurred;

  RefreshFeedHelper(this.newFeedItems, this.errorOccurred);
}