import 'package:drift/drift.dart';
import 'package:simple_feeds/src/database/tables.dart';
import 'package:simple_feeds/src/enums/feed_type.dart';
import 'package:simple_feeds/src/models/simple_feed_item.dart';
import 'package:webfeed/domain/atom_feed.dart';
import 'package:webfeed/domain/rss_feed.dart';

class SimpleFeed {
  final String title;
  final String? description;
  final String link;
  final String url;
  final FeedType feedType;
  final String? image;
  final List<SimpleFeedItem> simpleFeedItems;

  SimpleFeed(
      {required this.title,
      this.description,
      required this.link,
      required this.url,
      required this.feedType,
      this.image,
      required this.simpleFeedItems});

  SimpleFeed.fromAtomFeed(AtomFeed feed, this.url, {this.image})
      : title = feed.title ?? "",
        description = null,
        link = feed.links?.first.href ?? "",
        feedType = FeedType.ATOM,
        simpleFeedItems = feed.items
                ?.map((feedItem) => SimpleFeedItem.fromAtomFeedItem(feedItem))
                .toList() ??
            List.empty();

  SimpleFeed.fromRssFeed(RssFeed feed, this.url, {this.image})
      : title = feed.title ?? "",
        description = feed.description,
        link = feed.link ?? "",
        feedType = FeedType.RSS,
        simpleFeedItems = feed.items
                ?.map((feedItem) => SimpleFeedItem.fromRssFeedItem(feedItem))
                .toList() ??
            List.empty();

  FeedsCompanion convertToFeedsCompanion() {
    return FeedsCompanion(
        title: Value(title),
        description: Value(description),
        feedType: const Value(FeedType.RSS),
        link: Value(link),
        url: Value(url));
  }
}
