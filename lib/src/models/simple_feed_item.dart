import 'package:drift/drift.dart';
import 'package:simple_feeds/src/database/tables.dart';
import 'package:webfeed/domain/atom_item.dart';
import 'package:webfeed/domain/rss_item.dart';

class SimpleFeedItem {
  final String? title;
  final String? description;
  final String? link;
  final String? guid;
  final DateTime? publishDate;
  final DateTime dateReceived;
  final bool isRead;
  final bool isFavorite;

  SimpleFeedItem(
      {this.title,
      this.description,
      this.link,
      this.guid,
      this.publishDate,
      required this.dateReceived,
      required this.isRead,
      required this.isFavorite});

  SimpleFeedItem.fromRssFeedItem(RssItem item)
      : link = item.link,
        description = item.description,
        title = item.title,
        dateReceived = DateTime.now(),
        guid = item.guid,
        isFavorite = false,
        isRead = false,
        publishDate = item.pubDate;

  SimpleFeedItem.fromAtomFeedItem(AtomItem item)
      : link = item.links?.first.href,
        description = item.content,
        title = item.title,
        dateReceived = DateTime.now(),
        isFavorite = false,
        isRead = false,
        publishDate = item.updated,
        guid = null;

  FeedItemsCompanion convertToFeedItemsCompanion(int feedId) {
    return FeedItemsCompanion(
        link: Value(link),
        description: Value(description),
        title: Value(title),
        dateReceived: Value(DateTime.now()),
        guid: Value(guid),
        isFavorite: const Value(false),
        isRead: const Value(false),
        publishDate: Value(publishDate),
        feedId: Value(feedId));
  }
}
