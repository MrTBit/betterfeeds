import 'package:flutter/material.dart';
import 'package:simple_feeds/src/routing/route_state.dart';
import 'package:simple_feeds/src/screens/scaffold_body.dart';

class SimpleFeedsScaffold extends StatefulWidget {
  const SimpleFeedsScaffold({Key? key}) : super(key: key);

  @override
  SimpleFeedsScaffoldState createState() => SimpleFeedsScaffoldState();
}

class SimpleFeedsScaffoldState extends State<SimpleFeedsScaffold> {
  // bool removeRead = false;

  @override
  Widget build(BuildContext context) {
    final routeState = RouteStateScope.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(getAppBarText(routeState.route.pathTemplate)),
        actions: _getActions(routeState.route.pathTemplate),
      ),
      body: const SimpleFeedsScaffoldBody(),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              margin: EdgeInsets.zero,
              decoration: BoxDecoration(
                color: Theme.of(context).drawerTheme.backgroundColor,
              ),
              child: const Text('Simple Feeds'),
            ),
            const Divider(
              height: 0,
              thickness: 2,
            ),
            ListTile(
              title: const Text('Feeds'),
              selectedTileColor: Theme.of(context).bottomAppBarColor,
              selected: routeState.route.pathTemplate.startsWith('/feeds'),
              onTap: () {
                routeState.go('/feeds');
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Subscriptions'),
              selectedTileColor: Theme.of(context).bottomAppBarColor,
              selected:
                  routeState.route.pathTemplate.startsWith('/subscriptions'),
              onTap: () {
                routeState.go('/subscriptions');
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Settings'),
              selectedTileColor: Theme.of(context).bottomAppBarColor,
              selected: routeState.route.pathTemplate.startsWith('/settings'),
              onTap: () {
                routeState.go('/settings');
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  List<Widget>? _getActions(String route) {
    if (route.startsWith('/feeds')) {
      return [
        Row(
          children: [
           const Text("Unread"),
            Switch(
              value: SimpleFeedsScaffoldBody.feedsScreenStateKey.currentState?.removeReadState ?? false,
              onChanged: (value) {
                setState(() {
                  SimpleFeedsScaffoldBody.feedsScreenStateKey.currentState?.loadFeedItems(removeReadOverride: value);
                });
              },
            ),
          ],
        ),
      ];
    } else {
      return null;
    }
  }

  String getAppBarText(String route) {
    if (route.startsWith('/feeds')) {
      return 'Feeds';
    } else if (route.startsWith('/settings')) {
      return 'Settings';
    } else if (route.startsWith('/subscriptions')) {
      return 'Subscriptions';
    } else {
      return 'Simple Feeds';
    }
  }
}
