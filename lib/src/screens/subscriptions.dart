import 'dart:io';

import 'package:flutter/material.dart';
import 'package:simple_feeds/src/database/tables.dart';
import 'package:simple_feeds/src/dialogs/add_feed_dialog.dart';
import 'package:simple_feeds/src/enums/feed_type.dart';
import 'package:simple_feeds/src/services/feed_service.dart';

import '../../main.dart';

class SubscriptionsScreen extends StatefulWidget {
  const SubscriptionsScreen({Key? key}) : super(key: key);

  @override
  _SubscriptionsScreenState createState() => _SubscriptionsScreenState();
}

class _SubscriptionsScreenState extends State<SubscriptionsScreen> {
  late FeedService _feedService;

  List<Feed> _feeds = List.empty();

  @override
  Widget build(BuildContext context) => Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () => _addFeed(),
          backgroundColor: Theme.of(context).colorScheme.secondary,
          child: const Icon(Icons.add),
        ),
        body: ListView.separated(
          padding: const EdgeInsets.all(8),
          itemCount: _feeds.length,
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
              background: Container(
                color: Colors.red,
              ),
              direction: DismissDirection.horizontal,
              onDismissed: (direction) =>
                  removeSubscription(_feeds[index].id, index),
              key: UniqueKey(),
              child: Container(
                height: 75,
                color: Theme.of(context).colorScheme.surface,
                child: Row(
                  children: [
                    FutureBuilder(
                      future: _feedService.getSubscriptionImage(_feeds[index]),
                      builder:
                          (BuildContext context, AsyncSnapshot<File?> file) =>
                              SizedBox(
                        height: 71,
                        width: 71,
                        child: _getImageWidget(file),
                      ),
                    ),
                    Expanded(
                      child: Center(
                        child: Text(_feeds[index].title),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) => const Divider(
            height: 5,
          ),
        ),
      );

  @override
  void initState() {
    super.initState();
    _feedService = getIt<FeedService>();
    loadFeeds();
  }

  removeSubscription(int id, int index) async {
    _feeds.remove(_feeds[index]);
    await _feedService.removeSubscription(id);

    setState(() {});
  }

  loadFeeds() async {
    List<Feed> feeds = await _feedService.getAllFeeds();

    setState(() {
      _feeds = feeds;
    });
  }

  Future<void> _addFeed() async {
    AddFeedDialogModel? feedDialogModel = await showDialog(
        context: context, builder: (_) => const AddFeedDialog());

    if (feedDialogModel != null) {
      String? error;

      switch (feedDialogModel.feedType) {
        case FeedType.ATOM:
          error = await _feedService.addAtomFeed(feedDialogModel.url.trim());
          break;
        case FeedType.RSS:
          error = await _feedService.addRssFeed(feedDialogModel.url.trim());
          break;
        case null:
          break;
      }

      if (error != null) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text('An error has occurred, please try again.')));
      }

      loadFeeds();
    }
  }

  Widget _getImageWidget(AsyncSnapshot<File?> file) {
    if (file.hasData && file.data != null) {
      return Image.file(file.data!);
    } else {
      return const Icon(
        Icons.image,
        size: 40,
        color: Colors.white60,
      );
    }
  }
}
