import 'dart:io';
import 'package:flutter/material.dart';
import 'package:simple_feeds/main.dart';
import 'package:simple_feeds/src/database/tables.dart';
import 'package:simple_feeds/src/lifecycle.dart';
import 'package:simple_feeds/src/routing/route_state.dart';
import 'package:simple_feeds/src/services/feed_service.dart';
import 'package:url_launcher/url_launcher.dart';

class FeedsScreen extends StatefulWidget {
  const FeedsScreen({Key? key}) : super(key: key);

  @override
  FeedsScreenState createState() => FeedsScreenState();
}

class FeedsScreenState extends State<FeedsScreen> {
  late final FeedService _feedService;

  late final LifecycleEventHandler lifeCycleEventHandler = LifecycleEventHandler(resumeCallBack: () async => loadFeedItems());

  List<FeedItem> _feedItems = List.empty();
  bool removeRead = false;

  bool get removeReadState => removeRead;

  RouteState get _routeState => RouteStateScope.of(context);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Column(
          children: [
            Expanded(
              child: RefreshIndicator(
                child: ListView.separated(
                  padding: const EdgeInsets.all(8),
                  itemCount: _feedItems.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      height: 75,
                      color: Theme.of(context).colorScheme.surface,
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () => _launchUrl(_feedItems[index]),
                            child: FutureBuilder(
                              future: _feedService
                                  .getFeedItemImage(_feedItems[index]),
                              builder: (BuildContext context,
                                      AsyncSnapshot<File?> file) =>
                                  SizedBox(
                                height: 71,
                                width: 71,
                                child: _getImageWidget(file),
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () => _onFeedTapped(_feedItems[index]),
                              child: Center(
                                child: Text(
                                  _feedItems[index].title ?? "",
                                  style: TextStyle(
                                      color: _feedItems[index].isRead
                                          ? Colors.white54
                                          : Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(
                    height: 5,
                  ),
                ),
                onRefresh: _refreshFeeds,
              ),
            ),
          ],
        ),
      );

  void submit(String value) {
    _feedService.addRssFeed(value.trim());
  }

  @override
  void initState() {
    super.initState();
    _feedService = getIt<FeedService>();
    loadFeedItems();
    _refreshFeeds();

    WidgetsBinding.instance?.addObserver(lifeCycleEventHandler);
  }

  loadFeedItems({bool? removeReadOverride}) async {

    if (removeReadOverride != null) {
      removeRead = removeReadOverride;
    }

    List<FeedItem> items = removeRead
        ? await _feedService.getUnreadFeedItems()
        : await _feedService.getAllFeedItems();

    setState(() {
      _feedItems = items;
    });
  }

  Future _refreshFeeds() async {
    bool error = (await _feedService.refreshFeeds()).errorOccurred;

    if (error) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('An error occurred refreshing some/all feeds.')));
    }

    loadFeedItems();
  }

  Widget _getImageWidget(AsyncSnapshot<File?> file) {
    if (file.hasData && file.data != null) {
      return Image.file(file.data!);
    } else {
      return const Icon(
        Icons.image,
        size: 40,
        color: Colors.white60,
      );
    }
  }

  _launchUrl(FeedItem feedItem) async {
    if (feedItem.link != null) {
      await _feedService.markFeedItemAsRead(feedItem);
      await launch(feedItem.link!);
    }
  }

  _onFeedTapped(FeedItem feedItem) async {
    await _feedService.markFeedItemAsRead(feedItem);

    loadFeedItems();

    _routeState.go('/feeds/${feedItem.id}');
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(lifeCycleEventHandler);
    super.dispose();
  }
}
