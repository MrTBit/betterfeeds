import 'package:flutter/cupertino.dart';
import 'package:simple_feeds/src/routing/parsed_route.dart';
import 'package:simple_feeds/src/routing/route_state.dart';
import 'package:simple_feeds/src/screens/settings.dart';
import 'package:simple_feeds/src/screens/subscriptions.dart';
import 'package:simple_feeds/src/widgets/fade_transition_page.dart';

import 'feeds.dart';

///Displays the contents of the body of [SimpleFeedsScaffold]
class SimpleFeedsScaffoldBody extends StatelessWidget {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  static GlobalKey<FeedsScreenState> feedsScreenStateKey = GlobalKey<FeedsScreenState>();

  const SimpleFeedsScaffoldBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ParsedRoute currentRoute = RouteStateScope.of(context).route;

    //A nested Router isn't necessary because the back button behavior doesn't need to be customized
    return Navigator(
      key: navigatorKey,
      onPopPage: (route, dynamic result) => route.didPop(result),
      pages: [
        if (currentRoute.pathTemplate.startsWith('/feeds'))
          FadeTransitionPage<void>(child: FeedsScreen(key: feedsScreenStateKey,), key: const ValueKey('feeds'))
        else if (currentRoute.pathTemplate.startsWith('/settings'))
          const FadeTransitionPage<void>(child: SettingsScreen(), key: ValueKey('settings'))
        else if (currentRoute.pathTemplate.startsWith('/subscriptions'))
          const FadeTransitionPage<void>(child: SubscriptionsScreen(), key: ValueKey('subscriptions'))

        // Avoid building a Navigator with an empty `pages` list when the
        // RouteState is set to an unexpected path, such as /signin.
        //
        // Since RouteStateScope is an InheritedNotifier, any change to the
        // route will result in a call to this build method, even though this
        // widget isn't built when those routes are active.
        else
          FadeTransitionPage<void>(
            key: const ValueKey('empty'),
            child: Container(),
          ),
      ],
    );
  }
}