import 'package:flutter/material.dart';
import 'package:simple_feeds/src/routing/route_state.dart';
import 'package:simple_feeds/src/screens/feed_details.dart';
import 'package:simple_feeds/src/screens/scaffold.dart';
import 'package:simple_feeds/src/widgets/fade_transition_page.dart';

/// Builds the top-level navigator for the app. The pages to display are based
/// on the `routeState` that was parsed by the TemplateRouteParser.
class SimpleFeedsNavigator extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;

  const SimpleFeedsNavigator({
    required this.navigatorKey,
    Key? key,
  }) : super(key: key);

  @override
  _SimpleFeedsNavigatorState createState() => _SimpleFeedsNavigatorState();
}

class _SimpleFeedsNavigatorState extends State<SimpleFeedsNavigator> {

  final _scaffoldKey = const ValueKey('App scaffold');
  final _feedItemDetailsKey = const ValueKey('Feed item details screen');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final routeState = RouteStateScope.of(context);
    final pathTemplate = routeState.route.pathTemplate;

    int? feedItemIdSelected;
    if (pathTemplate == '/feeds/:feedItemId') {
      feedItemIdSelected =
          int.tryParse(routeState.route.parameters['feedItemId'] ?? "");
    }

    return Navigator(
      key: widget.navigatorKey,
      onPopPage: (route, dynamic result) {
        if (route.settings is Page &&
            (route.settings as Page).key == _feedItemDetailsKey) {
          routeState.go('/feeds');
        }

        return route.didPop(result);
      },
      pages: [
        FadeTransitionPage<void>(
          key: _scaffoldKey,
          child: const SimpleFeedsScaffold(),
        ),
        if (feedItemIdSelected != null)
          MaterialPage<void>(
            key: _feedItemDetailsKey,
            child: FeedItemDetailsScreen(feedItemId: feedItemIdSelected),
          ),
      ],
    );
  }
}
