import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:photo_view/photo_view.dart';
import 'package:simple_feeds/main.dart';
import 'package:simple_feeds/src/database/tables.dart';
import 'package:simple_feeds/src/services/feed_service.dart';
import 'package:simple_feeds/src/services/file_service.dart';

class FeedItemDetailsScreen extends StatefulWidget {
  final int? feedItemId;

  const FeedItemDetailsScreen({
    Key? key,
    this.feedItemId,
  }) : super(key: key);

  @override
  _FeedItemDetailsScreenState createState() => _FeedItemDetailsScreenState();
}

class _FeedItemDetailsScreenState extends State<FeedItemDetailsScreen> {
  late final FeedService _feedService;
  late final FileService _fileService;

  FeedItem? _selectedFeedItem;

  @override
  Widget build(BuildContext context) {
    if (_selectedFeedItem == null) {
      return const Scaffold(
        body: Center(
          child: Text("Feed Details Works"),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(_selectedFeedItem!.title ?? ""),
      ),
      body: SingleChildScrollView(
        child: Html(
          data: _selectedFeedItem?.description ?? "",
          onImageTap: (String? imgUrl, RenderContext contexta,
              Map<String, String> attributes, dom.Element? element) {
            if (imgUrl != null) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HeroPhotoViewRouteWrapper(
                            imageUrl: imgUrl,
                            feedId: _selectedFeedItem!.feedId,
                            feedItemId: _selectedFeedItem!.id,
                            fileService: _fileService,
                            minScale: 0.5,
                          )));
            }
          },
          customImageRenders: {
            networkSourceMatcher(): /*TODO: imagesSaved*/ true
                ? (rc, attributes, e) {
                    String? url = attributes["src"];

                    if (url != null) {
                      return FutureBuilder(
                        future: _fileService.loadFeedItemImage(url,
                            _selectedFeedItem!.feedId, _selectedFeedItem!.id),
                        builder:
                            (BuildContext context, AsyncSnapshot<File?> file) =>
                                _getImageWidget(file),
                      );
                    } else {
                      return const Icon(
                        Icons.image,
                        size: 40,
                        color: Colors.white60,
                      );
                    }
                  }
                : networkImageRender(),
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _feedService = getIt<FeedService>();
    _fileService = getIt<FileService>();
    _loadSelectedFeed();
  }

  _loadSelectedFeed() async {
    _selectedFeedItem = widget.feedItemId != null
        ? await _feedService.getFeedItemById(widget.feedItemId!)
        : null;

    setState(() {});
  }

  Widget _getImageWidget(AsyncSnapshot<File?> file) {
    if (file.hasData && file.data != null) {
      return Image.file(file.data!);
    } else {
      return const Icon(
        Icons.image,
        size: 40,
        color: Colors.white60,
      );
    }
  }
}

class HeroPhotoViewRouteWrapper extends StatelessWidget {
  const HeroPhotoViewRouteWrapper({
    Key? key,
    required this.imageUrl,
    required this.feedId,
    required this.feedItemId,
    required this.fileService,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
  }) : super(key: key);

  final String imageUrl;
  final int feedId;
  final int feedItemId;
  final FileService fileService;
  final BoxDecoration? backgroundDecoration;
  final dynamic minScale;
  final dynamic maxScale;

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: FutureBuilder(
            future: _getImageProvider(),
            builder: (BuildContext context,
                AsyncSnapshot<ImageProvider> imageProvider) {
              if (imageProvider.hasData) {
                return PhotoView(
                  imageProvider: imageProvider.data,
                  backgroundDecoration: backgroundDecoration,
                  minScale: minScale,
                  maxScale: maxScale,
                  heroAttributes: const PhotoViewHeroAttributes(tag: "someTag"),
                );
              } else {
                return const Text("Loading...");
              }
            }));
  }

  Future<ImageProvider> _getImageProvider() async {
    if (/*TODO: imagesSaved*/ true) {
      File? file =
          await fileService.loadFeedItemImage(imageUrl, feedId, feedItemId);
      if (file != null) {
        return FileImage(file);
      }
    }

    return NetworkImage(imageUrl);
  }
}
