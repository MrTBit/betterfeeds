import 'package:background_fetch/background_fetch.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_it/get_it.dart';
import 'package:simple_feeds/src/app.dart';
import 'package:simple_feeds/src/database/tables.dart';
import 'package:simple_feeds/src/services/feed_service.dart';
import 'package:simple_feeds/src/services/file_service.dart';
import 'package:simple_feeds/src/services/http_service.dart';
import 'package:simple_feeds/src/services/notification_service.dart';
import 'package:url_strategy/url_strategy.dart';

final getIt = GetIt.instance;
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> main() async {
  // Use to setHashUrlStrategy() to use "/#/" in the address bar (default). Use
  // setPathUrlStrategy() to use the path. You may need to configure your web
  // server to redirect all paths to index.html.
  //
  // On mobile platforms, both functions are no-ops.
  setHashUrlStrategy();

  setupGetIt();

  runApp(const SimpleFeeds());

  getIt<NotificationService>().initialize();

  BackgroundFetch.registerHeadlessTask(backgroundFetchHeadlessTask);
}

backgroundFetchHeadlessTask(HeadlessTask task) async {
  String taskId = task.taskId;
  bool isTimeout = task.timeout;
  if (isTimeout) {
    // This task has exceeded its allowed running-time.
    // You must stop what you're doing and immediately .finish(taskId)
    BackgroundFetch.finish(taskId);
    return;
  }
  setupGetIt(); //di gets lost in headless
  int newFeedItems = (await getIt<FeedService>().refreshFeeds()).newFeedItems;

  if (newFeedItems > 0) {
    await getIt<NotificationService>().displayNotification(newFeedItems);
  }

  BackgroundFetch.finish(taskId);
}

void setupGetIt() {
  getIt.registerSingleton<SimpleFeedsDatabase>(SimpleFeedsDatabase());
  getIt.registerSingleton<NotificationService>(NotificationService());
  getIt.registerSingleton<HttpService>(HttpService());
  getIt.registerSingleton<FileService>(FileService());
  getIt.registerSingleton<FeedService>(FeedService());
}
